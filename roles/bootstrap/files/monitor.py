#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import struct
import serial
from threading import Timer

class Monitor:
   def __init__(self,time=30,outfile="data.xml",dev='/dev/ttyUSB2',baud='115200'):
      self.mote = self._connectToMote(dev,baud)
      self.time = time
      self.running = False
      self.outfile = outfile

      #measurements:
      self.length = -1 #Length of frames we received
      self.measurements = 0 #number of measurements made
      self.rssi = 0 #avg of the rssi
      self.lqi = 0 #avg of the lqi
      self.recv_correct = 0 # number of correctly received frames
      self.recv_total = 0 #total number of frames that were sent to us (that we are aware of)

   def _connectToMote(self,dev,baud):
      try:
         return serial.Serial(dev,baud,timeout=1)
      except Exception as err:
         print "{0}".format(err)
         sys.exit(1)

   def _timeout(self):
      self.running = False

   def start(self):
      self.running = True
      timer = Timer(self.time,self._timeout)
      timer.start()
      rawFrame = []

      last_num = -1 #packet num we last saw
      bad_crc_num = 0 #number of sequential packets with bad CRC (between correct ones)
      while self.running:
         byte = self.mote.read(1)
         if byte != "":
            rawFrame += [ord(byte)]
   
         if rawFrame[-3:]==[0xff]*3 and len(rawFrame)>=7:
            (rxpk_len,rxpk_num,rxpk_rssi,rxpk_lqi,rxpk_crc) = \
            struct.unpack('>BBbBB', ''.join([chr(b) for b in rawFrame[-8:-3]]))

            self.measurements += 1
            if self.length == -1: #only set once, assume it does not change
               self.length = rxpk_len

            if rxpk_crc: #CRC correct
               self.recv_correct += 1 #increment correct received frames
               if last_num == -1:
                  self.recv_total += 1 #
               else:
                  result = rxpk_num - last_num
                  if result < 0:
                     result = (255-last_num) + (rxpk_num + 1) # +1 because rxpk starts at 0
                  self.recv_total += result - bad_crc_num
               last_num = rxpk_num
               bad_crc_num = 0
            else: #CRC false
               self.recv_total +=1 #received one, although crc failed
               bad_crc_num += 1 #log this bad crc frame
            
            #rssi & lqi
            if self.measurements == 1:
               self.rssi = rxpk_rssi
               self.lqi = rxpk_lqi
            else:
               #rssi
               rssi_running_avg = self.rssi * (self.measurements - 1)
               rssi_running_avg += rxpk_rssi
               self.rssi = rssi_running_avg / float(self.measurements)
               #lqi
               lqi_running_avg = self.lqi * (self.measurements - 1)
               lqi_running_avg += rxpk_lqi
               self.lqi = lqi_running_avg / float(self.measurements)
                  
	    #print rawFrame
            rawFrame = []
      self.output()


   def output(self):
      f = open(self.outfile,'w')
      f.write('<?xml version="1.0"?>')
      f.write('<data>')
      if self.recv_total == 0:
         f.write('<length> N/A </length>')
         f.write('<rssi> N/A </rssi>')
         f.write('<lqi> N/A </lqi>')         
         f.write('<pdr> N/A </pdr>')
      else:
         f.write('<length>' + str(self.length) + '</length>')
         f.write('<rssi>' + "{0:.1f}".format(self.rssi) + '</rssi>')
         f.write('<lqi>' + "{0:.1f}".format(self.lqi) + '</lqi>')
         f.write('<pdr>' + "{0:.2f}".format(self.recv_correct / float(self.recv_total)) + '</pdr>')
      f.write('</data>')
      # print "rssi: " + str(self.rssi)
      # print "lqi: " + str(self.lqi)
      # print "PDR: " + "{0:.2f}".format(self.recv_correct / float(self.recv_total))
      # print "output function called"
      f.close()


m = Monitor(float(sys.argv[1]),str(sys.argv[2]),str(sys.argv[3]))
#m = Monitor(5.0)
m.start()

