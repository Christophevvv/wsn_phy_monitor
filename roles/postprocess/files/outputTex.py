import os
import sys
import xml.etree.ElementTree as ET

filename = "output.tex"
duration = "unknown"
subGhz = False
try:
    filename = str(sys.argv[1])
    duration = str(sys.argv[2])
    subGhz = bool(sys.arg[3])
except:
    pass
output = open(filename,'w')

print("\documentclass{article}",file=output)
print("\\title{Generated measure results}",file=output)
print("\\begin{document}",file=output)
print("\maketitle",file=output)

spectrum = ""
if(subGhz):
    spectrum = "subGhz"
else:
    spectrum = "2.4 Ghz"

print("The following measurements were performed in the "+spectrum+" spectrum.",file=output)
print("Nodes were tested for a duration of " + duration + " seconds. See tx files for info on how many packets were sent per second.",file=output)

for sender in os.scandir("data"):
    print("\\begin{table}[h!]",file=output)
    print("\centering",file=output)
    print("\\begin{tabular}{ |c|c|c|c| }",file=output)
    print("\hline",file=output)
    print("\multicolumn{4}{|c|}{" + str(sender.name) + "} \\\\",file=output)
    print("\hline",file=output)
    print("\hline",file=output)    
    print("Receiver & PDR & RSSI & LQI \\\\",file=output)
    print("\hline",file=output)
    print("\hline",file=output)    
                 
    for receiver in os.scandir("data/"+str(sender.name)):
        if not receiver.is_file():
            continue #only process files
        #r = open("data/"+str(sender.name)+"/"+str(receiver.name),"r")
        tree = ET.parse("data/"+str(sender.name)+"/"+str(receiver.name))
        root = tree.getroot()
        rssi = root.find('rssi')
        if rssi == None:
            rssi = "N/A"
        else:
            rssi = rssi.text
        lqi = root.find('lqi')
        if lqi == None:
            lqi == "N/A"
        else:
            lqi = lqi.text
        pdr = root.find('pdr')
        if pdr == None:
            pdr == "N/A"
        else:
            pdr = pdr.text
            
        print(str(receiver.name)+" & "+str(pdr)+" & "+str(rssi)+" & "+str(lqi)+" \\\\",file=output)
        print("\hline",file=output)
        
    print("\end{tabular}",file=output)
    print("\caption{Physical layer measurement results " + str(sender.name) + "}",file=output)
    print("\end{table}",file=output)

print("\end{document}",file=output)        
