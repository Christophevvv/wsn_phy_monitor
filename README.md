This repository presents a way to automate measurement of the physical layer in a testbed setup.

## How to use:

- Get the inventory file (exportable from jFed)
- run *ansible-playbook -i ansible-hosts monitor.yml -f "number of nodes to test"*
- out comes a pdf file with the measurement results ( see group_vars/all.yml for changeable settings )
